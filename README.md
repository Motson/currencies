REST API для валют на Yii 2.

Источник данных: http://www.cbr.ru/scripts/XML_daily.asp.

**Методы**

- GET /v1/currencies —  возвращает список курсов валют с параметрами. Параметры:
    -   page - номер страницы
    -   per-page - кол-во элементов на странице
- GET /v1/currency/<id> — возвращает курс валюты для переданного id


API закрыто bearer-авторизацией. Токен: "100-token"

СТРУКТУРА ПРОЕКТА
-------------------

      commands/           команды
      config/             кофигурация
      models/             классы моделей
      runtime/            данные runtime
      modules/            модули приложения
      vendor/             пакеты поставщиков
      web/                точка входа с веб-ресурсами

ТРЕБОВАНИЯ
------------

1. Docker >= 17
2. Docker Compose >= 1.25


УСТАНОВКА
------------

### Установка и запуск с помощью Docker
    
Установка зависимостей

    docker-compose run --rm app composer install
    
Запуск контейнера

    docker-compose up -d
    
Приложение доступно по ссылке:

    http://127.0.0.1:8080


КОНФИГУРАЦИЯ
-------------

### База данных

Выполните миграции для БД:
```
docker-compose run --rm app php yii migrate
```

### Загрузка данных по валютам

Выполните команду для загрузки данных по валютам
```
docker-compose run --rm app php yii currency/update
```


ТЕСТЫ
-------
Для юнит тестов и тестирования api используется _Codeception v4_. 
Запуск тестов:

```
docker-compose run --rm app vendor/bin/codecept run
```