<?php

use yii\db\Migration;

/**
 * Class m201016_073909_init_curency
 */
class m201016_073909_init_curency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('currency', [
            'id' => $this->primaryKey(),
            'code' => $this->string()->comment('Код валюты'),
            'name' => $this->string()->comment('Название валюты'),
            'rate' => $this->double()->comment('Курс валют к рублю'),
        ]);
        $this->createIndex('code_index', 'currency', 'code');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('currency');
    }
}
