<?php

namespace app\modules\v1\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\Response;
use app\models\Currency;
use yii\data\ActiveDataProvider;

class CurrencyController extends Controller
{
    /**
     * Вывод списка валют
     * @uses $_GET['page'] Номер страницы
     * @uses $_GET['per-page'] Кол-во на странице
     * @return array
     */
    public function actionIndex() {
        $provider = new ActiveDataProvider([
            'query' => Currency::find(),
        ]);

        return [
            'data' => $provider->getModels(),
            'totalCount' => $provider->getTotalCount(),
        ];
    }

    /**
     * Вывод валюты по id
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     * @throws \yii\web\HttpException
     */
    public function actionView($id) {
        $currency = Currency::find()->where(['id' => $id])->one();
        if(!$currency) {
            throw new \yii\web\HttpException(404, 'Not found');
        }
        return $currency;
    }
}
