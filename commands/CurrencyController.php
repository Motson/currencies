<?php
/**
 * Обновление данных по валютам
 */
namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Currency;

class CurrencyController extends Controller
{
    public function actionUpdate() {
        $currencyList = $this->getCurrencies();
        if($currencyList['Valute']) {
            foreach($currencyList['Valute'] as $inputCurrency) {
                $currency = Currency::find()->where(['code' => $inputCurrency['CharCode']])->one();
                if(empty($currency)) {
                    $currency = new Currency();
                    $currency->code = $inputCurrency['CharCode'];
                    $currency->name = $inputCurrency['Name'];
                }
                $currency->rate = (float)str_replace(',', '.', $inputCurrency['Value']);
                $currency->save();
            }
        }

        return ExitCode::OK;
    }

    protected function getCurrencies() {
        $string = file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp');
        $xml = simplexml_load_string($string);
        $json = json_encode($xml);
        $currencyList = json_decode($json, true);
        return $currencyList;
    }
}
