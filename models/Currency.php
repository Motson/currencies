<?php

namespace app\models;

use Yii;
use yii\behaviors\AttributeTypecastBehavior;

/**
 * Класс модели для валюты
 *
 * @property int $id
 * @property string|null $code Внешний код валюты
 * @property string|null $name Название валюты
 * @property float|null $rate Курс валют к рублю
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['rate'], 'number'],
            [['code', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'code' => 'Код валюты',
            'name' => 'Имя',
            'rate' => 'Курс валюты к рублю',
        ];
    }
}
