<?php 

class CurrencyCest
{
    const CHECK_CURRENCY = [
        'id' => 1,
        'code' => 'AUD',
    ];

    public function _before(ApiTester $I) {
        $I->haveHttpHeader('Authorization', "Bearer 100-token");
    }

    public function currenciesTest(ApiTester $I) {
        $I->sendGet("currencies");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        // Тест пагинации и наличия валют
        $I->seeResponseMatchesJsonType([
            'totalCount' => 'integer:>0'
        ]);
        // Тест первой валюты
        $I->seeResponseContainsJson([
            'data' => self::CHECK_CURRENCY,
        ]);
    }

    public function currencyByIdTest(ApiTester $I) {
        $I->sendGet("currencies/" . self::CHECK_CURRENCY['id']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(self::CHECK_CURRENCY);
    }
}
