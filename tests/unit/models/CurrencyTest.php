<?php namespace models;

use app\models\Currency;

class CurrencyTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testFindById() {
        expect_that($currency = Currency::find()->where(['id' => 1])->one());
        expect($currency->name)->equals('Австралийский доллар');
        expect($currency->rate)->greaterThan(0);
    }
}